provider "aws" {
  region = "us-west-2"
}
resource "aws_ebs_encryption_by_default" "ebsDefaultEncryption" {
  enabled = false
}

resource "aws_s3_bucket" "demo-example" {
  bucket = "demoexample-1"
  versioning {
    enabled = false
    mfa_delete = false
  }
}
